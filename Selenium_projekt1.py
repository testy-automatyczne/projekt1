import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver.support.select import Select

#Dane testowe
login = "standard_user"
password = "secret_sauce"
firstname = "Jan"
lastname = "Kowalski"
postcode = "00-001"

class test(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.get("https://www.saucedemo.com/")
        self.driver.implicitly_wait(40)

    def testNoNameEntered(self):
        # KROKI
        driver = self.driver
        # 1. Wpisanie loginu
        login_input = driver.find_element(By.ID, 'user-name')
        login_input.send_keys(login)
        # 2. Wpisanie hasła
        password_input = driver.find_element(By.ID,'password')
        password_input.send_keys(password)
        # 3. Klikniecie pzycisku "LOGIN"
        driver.find_element(By.ID, "login-button").click()
        # 4. Wybór sortowania
        select = Select(driver.find_element(By.CLASS_NAME, "product_sort_container"))
        select.select_by_visible_text("Price (low to high)")
        # 5. Wybór pierwszego produktu i powrót
        driver.find_element(By.CLASS_NAME, "inventory_item_name").click()
        driver.find_element(By.ID, "add-to-cart-sauce-labs-onesie").click()
        driver.find_element(By.ID, "back-to-products").click()
        #6. Ponowny wybór listy i wybranie następnego produktu
        select = Select(driver.find_element(By.CLASS_NAME, "product_sort_container"))
        select.select_by_value('za')
        # 7. Wybór drugiego produktu
        driver.find_element(By.ID, "add-to-cart-sauce-labs-bike-light").click()
        # 8. Przejście do koszyka
        driver.find_element(By.CLASS_NAME, "shopping_cart_link").click()
        # 9. Zapłata
        driver.find_element(By.ID, "checkout").click()
        # 10. Wypełnienie danych
        firstname_input = driver.find_element(By.ID, "first-name")
        firstname_input.send_keys(firstname)
        lastname_input = driver.find_element(By.ID, "last-name")
        lastname_input.send_keys(lastname)
        postcode_input = driver.find_element(By.ID, "postal-code")
        postcode_input.send_keys(postcode)
        # 11. złożenie zamówienia
        driver.find_element(By.ID, "continue").click()
        driver.find_element(By.ID, "finish").click()
        # 12. Powrót i wylogowanie
        driver.find_element(By.ID, "back-to-products").click()
        driver.find_element(By.ID, "react-burger-menu-btn").click()
        driver.find_element(By.ID, "logout_sidebar_link").click()

        sleep(10)

    def tearDown(self):
        self.driver.quit()